package kursova;

import java.util.Scanner;
public class Laba3 {
    public static void main(String[] args) {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        int[] f = new int[26];
        System.out.println("Enter a string.");
        String k = sc.nextLine();
        String s = k.toUpperCase();
        s = s.trim();
        int l = s.length();
        System.out.println("Checking string = " + s);
        char ch;
        for (int i = 0; i < l; i++) {
            ch = s.charAt(i);

            //This will give the ASCII value of the character i.e. ch
            int temp=(int)ch;
            if(temp>=65 && temp<=90){
                //subtract 65 to get index
                //add 1 to increase frequency
                f[temp - 65]+=1;
            }

        }
        System.out.println("Char\tFreq");
        for (int i = 0; i < 26; i++) {
            if (f[i] != 0) {
                //Add 65 to get respective character
                System.out.println((char)(i+65) + "\t" + f[i]);
            }
        }
    }
}